## [1.0.1](https://github.com/yamadharma/kermit-makefiles/compare/v1.0.0...v1.0.1) (2021-07-22)


### Bug Fixes

* **beamer:** add aspect ratio 16x10 to beamer class ([fc5ae38](https://github.com/yamadharma/kermit-makefiles/commit/fc5ae386d31d26d3f74c43f409643f4f1280ac41))



# [1.0.0](https://github.com/yamadharma/kermit-makefiles/compare/v0.4.26...v1.0.0) (2021-07-22)


### Features

* add fefault options for polyglossia russia ([10c8009](https://github.com/yamadharma/kermit-makefiles/commit/10c8009362f3e2c26e8cdbf2695c49d39204c39e))
* add mmap package ([5c71ca9](https://github.com/yamadharma/kermit-makefiles/commit/5c71ca92f2872c7dc2939ab191ff8eb4ace3f9de))
* change scale of PT Mono font ([1ec435f](https://github.com/yamadharma/kermit-makefiles/commit/1ec435f61f196cbb73c65705d214a7550baaf3eb))
* **fonts:** add paratype+noto fontset ([e4bfec2](https://github.com/yamadharma/kermit-makefiles/commit/e4bfec2f2bda8480e8bb90205065756c866ca086))
* **presentation:** add size 16x10 for presentations ([5d7b5cc](https://github.com/yamadharma/kermit-makefiles/commit/5d7b5ccbfb2cc4e8afff05797381c3af5121579f))
* **presentation:** change directories for presentation ([68dfdf0](https://github.com/yamadharma/kermit-makefiles/commit/68dfdf007651f8a9b8b894faf8b2b3fe11305898))




